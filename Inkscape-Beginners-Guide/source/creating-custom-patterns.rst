************************
Creating Custom Patterns
************************

You can make your own patterns in Inkscape. A pattern can consist of a texture
(i.e. a raster image like \*.jpg or \*.png that you imported), a group, a path,
shape objects, …

Select the object that you would like use as a pattern, and then tell the
software to turn it into a pattern via :menuselection:`Object --> Pattern -->
Objects to Pattern`. Your pattern will appear on the canvas, applied to a
rectangle. It will now also be available in the :guilabel:`Fill and Stroke`
dialog, next to the stock patterns that come with Inkscape.

The :guilabel:`Pattern to Objects` option in the same menu allows you to do the
opposite operation. This is useful when you want to modify a pattern, or want to
extract an object from it.

Your first patterns may look a little surprising to you. When you start
designing beautiful seamless patterns, you are engaging in the art of
illustration.

.. figure:: images/custom_pattern.png
   :alt:
   :class: screenshot

   This octopus image is using a custom fish pattern.
