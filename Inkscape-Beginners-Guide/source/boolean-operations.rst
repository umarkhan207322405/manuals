******************
Boolean Operations
******************

The Boolean Operations work on paths, or they try to convert the selected
objects to paths before they compute the result. All these operations need at
least 2 convertible objects or paths, which will be combined following specific
rules.

|Union icon| Union
  Keeps the common outline of all selected
  paths.

|Difference icon| Difference
  Subtracts one path from another one.

|Intersection icon| Intersection
  Only keeps those parts that are
  covered by all selected paths.

|Exclusion icon| Exclusion
  Keeps those parts which are covered by an uneven number of paths (if you have
  two objects, this is where the objects do not overlap).

|Division icon| Division
  The path below is cut into pieces by the path above.

|Cut path icon| Cut Path
  Creates as many paths as there are path intersections between the two paths.

|Combine icon| Combine
  Keeps all parts, and combines them into a single object.

|Break apart icon| Break Apart
  If a path consists of a number of independent parts (subpaths), this will
  create that number of separate objects.

To use these operations, select the two (or more) objects, and then select the
option of your choice in the :guilabel:`Path` menu. The result will immediately
appear on the canvas - if it doesn't, read the error message that will appear in
the status bar, at the bottom of the Inkscape window, to find out about the
reason for the failure.

.. figure:: images/boolops_union_1.png
   :alt: Two objects about to be unioned
   :class: screenshot

   Two objects about to be unioned

.. figure:: images/boolops_union_2.png
   :alt: The result of unioning the triangle and the square
   :class: screenshot

   Unioning a triangle and a square gives a house.

.. figure:: images/boolops_difference_1.png
   :alt: The rectangle will become the door opening.
   :class: screenshot

   The rectangle will become the door opening.

.. figure:: images/boolops_difference_2.png
   :alt: The door is open
   :class: screenshot

   Difference between a rectangle and a house creates an opening for the
   door.

.. figure:: images/boolops_intersection_1.png
   :alt: Two overlapping ellipses
   :class: screenshot

   Two overlapping ellipses

.. figure:: images/boolops_intersection_2.png
   :alt: Intersection between the two ellipses
   :class: screenshot

   Intersection between the two ellipses

.. figure:: images/boolops_exclusion.png
   :alt: Exclusion between the two ellipses
   :class: screenshot

   Exclusion between the two ellipses.

.. figure:: images/boolops_division_1.png
   :alt: Ellipse with a path on top
   :class: screenshot

   Someone has drawn a path with the pencil tool (with the setting of
   :guilabel:`Shape: Ellipse`) on the orange ellipse.

.. figure:: images/boolops_division_2.png
   :alt: Ellipse divided by the path
   :class: screenshot

   Division

.. figure:: images/boolops_combine.png
   :alt: The two parts of the ellipse have been combined into a single path  (with subpaths)
   :class: screenshot

   Move apart and combine (to form a single path composed of two subpaths).

.. figure:: images/boolops_break_apart.png
   :alt: Each subpath has become a single path after Break Apart.
   :class: screenshot

   :guilabel:`Break Apart` separates all subpaths into independent objects.



.. |Union icon| image:: images/icons/path-union.*
   :class: inline
.. |Difference icon| image:: images/icons/path-difference.*
   :class: inline
.. |Intersection icon| image:: images/icons/path-intersection.*
   :class: inline
.. |Exclusion icon| image:: images/icons/path-exclusion.*
   :class: inline
.. |Division icon| image:: images/icons/path-division.*
   :class: inline
.. |Cut path icon| image:: images/icons/path-cut.*
   :class: inline
.. |Combine icon| image:: images/icons/path-combine.*
   :class: inline
.. |Break apart icon| image:: images/icons/path-break-apart.*
   :class: inline
