****************************
Installing Inkscape on a Mac
****************************

For Inkscape to run properly on a computer with a macOS / OS X operating system, you will first need to install XQuartz, then Inkscape.

Using XQuartz
=============

This is the recommended installation method for macOS users.

Step 1: Install XQuartz
-----------------------

#. Using a web browser, go to the `XQuartz website <https://www.xquartz.org/>`_.

   |image0|

#. Click the XQuartz dmg icon to download it.

   |image1| 

#. Once it's fully downloaded, go to the folder your browser is set for
   files to be downloaded into. Usually this is the :file:`Downloads` folder
   (:file:`System/Users/username/Downloads`). Double-click the XQuartz.dmg
   file to open it.

   |image2|
    
#. A new window will appear. Double-click the XQuartz.pkg icon to open
   it.

   |image3| 
    
#. Another new window, which enables you to install XQuartz, will
   appear. Follow the steps and instructions in this window.

   |image4|
    
#. Restart your Mac. You can either shut down or log out of it.

   |image5|

   |image6|

Step 2: Install Inkscape
------------------------

#. Using a web browser, go to the page in Inkscape.org where you
   download the Inkscape installer for the Mac.

   |image7| 
    
#. Click the box labelled :guilabel:`Mac OS X 10.7 Installer (xQuartz)` to
   download the .dmg file, which contains this installer.

   |image8| 
    
#. A new window showing the installer downloading will appear in your
   browser window.

   |image9|
    
#. Once it's fully downloaded, go to the folder your browser is set for
   files to be downloaded into. Usually this will be the :file:`Downloads` folder
   (:file:`System/Users/username/Downloads`). Double-click the installer
   to open it.

   |image10|
    
#. Click and drag the Inkscape icon to the Applications icon as
   instructed in this window. This will install Inkscape to your Mac.

   |image11|

Step 3: Setting Up Inkscape
---------------------------

#. Open Inkscape by double-clicking its icon in the :guilabel:`Applications` folder.

   |image12|
    
#. Click OK in this window, which appears the first time Inkscape is
   opened.

   |image13|
    
#. Wait for Inkscape to open. This might take a few minutes, since
   Inkscape is scanning all the font files in your system. The next time you open
   Inkscape, it will not take nearly as long to show up. As soon as
   Inkscape does open, its interface will appear inside XQuartz.

   |image14|
    
#. Open Inkscape's/XQuartz's preferences.

   |image15|
    
#. Click the Input tab, and configure its settings to this.

   |image16|
    
#. Click the Pasteboard tab and configure its settings to this.

   |image17|
    
#. Close Preferences. You are now ready to use Inkscape.

Other Ways to Install Inkscape
==============================

You can also build Inkscape in your Mac using Homebrew or MacPorts.

Using Homebrew
--------------

#. Install Homebrew by doing the following:

   #. Open :guilabel:`Terminal` in :menuselection:`System --> Applications --> Utilities`.

   |image18| 
       
   #. In Terminal, type:

      ::

        /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
      

   #. In Terminal, do either of the following:

      - If you've never installed Inkscape with Homebrew before, type:

        ::

          brew install caskformula/caskformula/inkscape

           

      - If you have installed Inkscape with Homebrew before, type:

        ::

          brew uninstall inkscape
          brew cleanup

Using MacPorts
--------------

#. Install Xcode. Read `this
   guide <https://guide.macports.org/#installing.xcode>`_
   on how to do it, since there are different ways of installing Xcode
   for different versions of OS X.

   |image19|
    
#. Install MacPorts. Read `the
   guide <https://guide.macports.org/#installing.macports>`_
   on how to do it, since there are different ways of installing
   MacPorts for different versions of OS X.

   |image20|
    
#. Open :guilabel:`Terminal` in :menuselection:`System --> Applications --> Utilities`.

   |image21|
    
#. In Terminal, type either of the following:

   -  For the X11 version of Inkscape:

      ::

          sudo port install inkscape

   -  For the Quartz version of Inkscape (be warned this might be less
      stable):

      ::

          sudo port install inkscape +quartz 

.. |image0| image:: images/install_xquartz_gotosite.png
.. |image1| image:: images/install_xquartz_download_xq.png
.. |image2| image:: images/install_xquartz_open_xq_dmg.png
.. |image3| image:: images/install_xquartz_open_xq_pkg.png
.. |image4| image:: images/install_xquartz_wizard.png
.. |image5| image:: images/install_xquartz_shut_down.png
.. |image6| image:: images/install_xquartz_log_out.png
.. |image7| image:: images/install_inkscape_gotosite.png
.. |image8| image:: images/install_inkscape_download_is.png
.. |image9| image:: images/install_inkscape_is_downloading.png
.. |image10| image:: images/install_inkscape_open_is_dmg.png
.. |image11| image:: images/install_inkscape_is_to_applications_folder.png
.. |image12| image:: images/set_up_inkscape_open_is.png
.. |image13| image:: images/set_up_inkscape_fyi_click_ok.png
.. |image14| image:: images/set_up_inkscape_is_interface.png
.. |image15| image:: images/set_up_inkscape_open_is_preferences.png
.. |image16| image:: images/set_up_inkscape_input_prefs.png
.. |image17| image:: images/set_up_inkscape_pasteboard_prefs.png
.. |image18| image:: images/macosx_terminal_location.png
.. |image19| image:: images/install_inkscape_macports_xcode.png
.. |image20| image:: images/install_inkscape_macports_mp.png
.. |image21| image:: images/macosx_terminal_location.png
