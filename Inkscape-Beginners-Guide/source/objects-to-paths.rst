************************************
Editing Nodes on a Geometrical Shape
************************************

There are multiple ways to modify an object in your drawing. If the object is a
rectangle, an ellipse, a star or a spiral, i.e. a geometrical shape drawn with
one of the shape tools, then this object is a **shape**, and not a **path**.

Shapes can only be modified by dragging their specific handles. In contrast to
shapes, you will edit a path with tools that are meant for modifying paths.

However, **Shapes can be converted into paths**, so the path tools can then be
used with them.

To convert a shape into a path:

#. Select the shape with the Selector tool.
#. In the menu, select :menuselection:`Path --> Object to path`.

.. WARNING::
   A shape can always be converted to a path, but a path can never
   be converted back into an object!
