****************
About This Guide
****************

This book was originally created by **Elisa de Castro Guerra**, designer and
`educational instructor <http://activdesign.eu/>`_. It was created as a reference guide for learning Inkscape.

It is available under a Creative Commons license (`CC-By-SA
<https://creativecommons.org/licenses/by-sa/4.0/>`_) and was first published on TO BE DETERMINED.

The book was translated from French to English in 2016-2017.

The version of the book that you are reading now has been updated and adapted by
Inkscape community volunteers, to match Inkscape 1.0.

We sincerely hope you found this little guide helpful when making
your first steps with Inkscape, and that you will enjoy drawing in Inkscape just
as we do. If you find something that could be improved, or needs to be updated,
or have any other suggestions concerning this book, we'd be happy to have you
join us at our `collaboration space at gitlab.com
<https://gitlab.com/inkscape/inkscape-docs/manuals>`_ or on `Inkscape's
documentation mailing list
<https://lists.inkscape.org/postorius/lists/inkscape-docs.lists.inkscape.org/>`_

These are the people who contributed to this book:

- Jabiertxo Arraiza Cenoz
- brynn
- Elisa de Castro Guerra
- Hinerangi Courtenay
- Sylvain Chiron
- Sergio González Collado
- Maren Hachmann
- Rosalind Lord
- JP Otto
- Jon Peyer
- Christopher Michael Rogers
- Carl Symons
- Reidar Vik
- Marietta Walker
- *Help us to improve this guide! Then you will find your own name in this list :-)*
