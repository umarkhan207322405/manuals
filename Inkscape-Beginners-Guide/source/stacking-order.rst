**************
Stacking Order
**************

If you have drawn objects that overlap each other, you can select on object and then click "Raise" or "Lower" under the "Object" menu to change the stacking order.
