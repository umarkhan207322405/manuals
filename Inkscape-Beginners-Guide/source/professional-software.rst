*********************
Professional Software
*********************

Inkscape is powerful but doesn't require a recent, high-end computer to be usable. It has many drawing tools, filters, effects and patterns.

Many professionals use Inkscape for vector image editing. It fits easily into a design workflow with open source or proprietary tools.

How much does it cost?
======================

Inkscape is **free and open source software** (`Wikipedia <https://en.wikipedia.org/wiki/Free_and_open-source_software>`_). It can be `downloaded <https://inkscape.org/releases/>`_ and installed on :doc:`Windows <installing-on-windows>`, :doc:`Mac <installing-on-mac>`, and :doc:`Linux <installing-on-linux>`. Copying, distribution, and modification of the software are freely permitted. Inkscape can be used at work or at home, for professional or personal work. The only investment you'll need to make is to **learn** to use it and gain **new skills**.

You can also `donate <https://inkscape.org/support-us/>`_ to allow the project volunteers to dedicate more time to it.
