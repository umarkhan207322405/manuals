***********************
Creating Custom Markers
***********************

Just as you can create custom patterns, you can also create your own custom
markers. In order to do so, draw the marker you're envisioning, then make it
available for use by doing :menuselection:`Object --> Object to markers`.

Your marker is now available at the top of the list of markers in the drop-down
menu (:guilabel:`Fill and Stroke` dialog, third tab).

.. figure:: images/custom_markers_step1.png
   :alt: Markers and path
   :class: screenshot

   We have drawn our markers and our path. For the ends, in this case we want to have two mirrored markers.

.. figure:: images/custom_markers_step2.png
   :alt: Convert to marker
   :class: screenshot

   Now we convert the object to a marker. It will be gone from the drawing after this operation.

.. figure:: images/custom_markers_step3.png
   :alt: Markers applied
   :class: screenshot

   In the :guilabel:`Fill and Stroke` dialog, we have selected the new markers for the start, end and middle of our path. They are placed on the nodes of the path. The size of the markers depends on the size of the objects that were converted to a marker. It also changes with the stroke width of the path.
