**************************
When do you need Inkscape?
**************************

There are many image creation and manipulation software programs.

However there are two main types of images:

-  **Raster** or **bitmap** images such as images from digital cameras.
   They are made up of :term:`pixels <Pixel>`, and the more the images are
   manipulated, the more the quality declines. Zooming or shrinking can cause
   blurriness or pixelation. Raster images can contain millions of colors and look very realistic.
-  **Vector** images are created with vector graphics software. They are
   made up of mathematically defined paths, and are independent of the
   resolution–the image adapts to the available space without losing quality.

   This is why the same vector image can be used for different sizes of the
   final image presentation. For example, the same image can be used for a
   business card or a large poster with equal quality.

   Vector images often have an artificial and clean look, if they are used for drawing realistically. They consist of separate objects, each of which can be modified separately. Each object has its own style (color, patterns, etc.).

Depending on how the image is used, either raster or vector graphics may
suit better to your needs.

**Inkscape** is a **program for creating and editing vector graphics**.

It is the ideal tool for drawing logos and icons, creating (animatable) graphics for websites, for composing posters and flyers, or for making patterns for use with cutting machines and laser engravers.

It is also a good choice for drawing short comics, making art, building user interface mockups and making or editing diagrams created by other software. People use it for fun projects, such as map-making, creating birthday cards, painting on eggs, 2D game graphics and for **many, many more purposes**.

Inkscape is **not the ideal tool** for some other purposes, but there exist very powerful alternative free software programs which we recommend without reservations:

- **Photo editing** (raster graphics): `Gimp <https://gimp.org>`_
- **Desktop publishing** (multi-page documents, printing in CMYK color space): `Scribus <https://scribus.net>`_
- **CAD** (parametrical construction, engineering): `LibreCAD (2D) <https://librecad.org/>`_
- **Painting** (raster graphics): `Krita <https://krita.org>`_

Here are some examples of the types of images and projects that can be made with Inkscape:

Artwork and Comics
==================

.. image:: images/examples/WINTERFALL_by_Rizki_Ilman_Hidayanto_DzunnunSulaiman-PD.png
   :width: 22%
.. image:: images/examples/inkscape-island-of-creativity_by_Bayu_Rizaldhan_Rayes_CC-BY-SA.png
   :width: 22%
.. image:: images/examples/COCOART_by_Rizki_Ilman_Hidayanto_DzunnunSulaiman-PD.png
   :width: 22%
.. image:: images/examples/InkboardDoF_by_Piotr_Milczanowski-CC_BY_SA.png
   :width: 22%
.. image:: images/examples/InkscapeMan_Final_by_Tim_Jones-CC_BY_SA.png
   :width: 22%
.. image:: images/examples/cartoon-092_by_GD-CC_BY_SA.png
   :width: 22%
.. image:: images/examples/snail_the_alien_by_Danang_Bimantoro-PD.png
   :width: 22%
.. image:: images/examples/Elvie_056_en-GB_www.peppertop.com_CC-BY-SA.jpg
   :width: 22%


Clip art, Icons and Logos
=========================

.. image:: images/examples/baby-narwhal_Martin_Owens_CC-BY-SA.png
   :width: 22%
.. image:: images/examples/bug_induced_by_Alexey_Kyurshunov-CC_BY.png
   :width: 22%
.. image:: images/examples/Spell_Book_Equinox3141_CC-BY-SA.png
   :width: 22%
.. image:: images/examples/bug_documentation_by_Alexey_Kyurshunov-CC_BY.png
   :width: 22%
.. image:: images/examples/lucentis_by_Cristian_Pozzessere_CC_By_SA.png
   :width: 22%
.. image:: images/examples/x_by_theddy_grumo_PD.png
   :width: 22%
.. image:: images/examples/Moon_by_desmafagafado_PD.png
   :width: 22%
.. image:: images/examples/logo_stamp_r02_CC_BY_SA.png
   :width: 22%

Layout and Prototyping
======================

.. image:: images/examples/FreiesMagazin_April_2015_by_Maren_Hachmann-CC_BY_SA.jpg
   :width: 22%
.. image:: images/examples/slide-gnu_Roger_Condo_Ochoa_CC-BY.png
   :width: 22%
.. image:: images/examples/Gimp_Inkscape_Banners_by_Ryan_Gorley_CC_By-SA.jpg
   :width: 22%
.. image:: images/examples/0002-BLOG_IDA-MUZAIKO_Eder_Benedetti_CC-BY.png
   :width: 22%
.. image:: images/examples/Concept_art_pack_Mypaint_Ramon_Miranda_PD.png
   :width: 22%
.. image:: images/examples/telescopica_mobile_by_Marien_Soria-CC_BY_SA.png
   :width: 22%
.. image:: images/examples/UI_Elements_hayavadhan-CC_BY.png
   :width: 22%
.. image:: images/examples/Logo_RM_and_tshirt_by_Ramon_Miranda_CC_By_SA.png
   :width: 22%

Technical Drawings and Diagrams
===============================

.. image:: images/examples/Mohrs_circle_by_Johannes_Kalliauer-PD.png
   :width: 22%
.. image:: images/examples/mtDNA_by_sdjbrown-CC_BY.png
   :width: 22%
.. image:: images/examples/Origami_Tux_by_Dario_Badagnani-CC-BY-SA.jpg
   :width: 22%
.. image:: images/examples/croquis_afrique_sans_nom_by_flanet-PD.png
   :width: 22%

Physical Objects
================

.. image:: images/examples/Commodore_VC1520_plotter_extension_for_Inkscape_by_Johan_Van_den_Brande_MIT.jpg
   :width: 22%
.. image:: images/examples/cookiecutter_by_Alexander_Pruss_MIT.png
   :width: 22%
.. image:: images/examples/Widgets_Paris_by_Martin_Owens_CC_BY_SA.JPG
   :width: 22%


.. image:: images/vector-format.png
   :class: deco center medium
